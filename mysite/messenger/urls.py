from django.urls import path
from django.views.generic import RedirectView

from .views import index, IndexView

app_name = 'messenger'

urlpatterns = [
    # Dossiers
    path('inbox/', index, name='inbox'),
    path('sent/',  index, name='sent'),

    # Actions
    path('write/', index, name='write'),
    path('view/<int:id>/', index, name='view'),
    path('delete/', index, name='delete'),

    # path('', RedirectView.as_view(
    #     pattern_name = 'messenger:inbox', permanent=True)),
    # Remplacé par :
    path('', IndexView.as_view()), # Redirige sur INBOX
]