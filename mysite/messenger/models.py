from django.db import models
from django.conf import settings
from django.utils.timezone import now

# Create your models here.
class Message(models.Model):
    subject = models.CharField("sujet",max_length=120)
    body = models.TextField("corps", blank=True)
    sender = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE,
                               related_name='sent_messages',
                               verbose_name="expéditeur")
    recipient = models.ForeignKey(settings.AUTH_USER_MODEL,
                                  on_delete=models.CASCADE,
                                  related_name='received_messages',
                                  verbose_name="destinataire")